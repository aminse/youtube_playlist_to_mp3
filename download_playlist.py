import bs4
import sys
import urllib.request
from PyQt5.QtWebEngineWidgets import QWebEnginePage
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QUrl
import pytube  # library for downloading youtube videos
import os
from moviepy.editor import *
import glob

class Page(QWebEnginePage):
    def __init__(self, url):
        self.app = QApplication(sys.argv)
        super().__init__()
        self.html = ""
        self.loadFinished.connect(self._on_load_finished)
        self.load(QUrl(url))
        self.app.exec_()

    def _on_load_finished(self):
        self.html = self.toHtml(self.Callable)
        print("Load finished")

    def Callable(self, html_str):
        self.html = html_str
        self.app.quit()


def extract_links(url):
    # Scrape and extract video links from the given playlist url
    page = Page(url)
    soup = bs4.BeautifulSoup(page.html, "html.parser")
    links = []
    for link in soup.find_all("a", id="thumbnail"):
        # Ignore the first link, which is the playlist link
        if link != soup.find_all("a", id="thumbnail")[0]:
            try:
                vid_src = link["href"]
                # Format the link to be given to pytube
                new_link = exact_link(vid_src)
                links.append(new_link)
            except KeyError:
                pass  # Ignore invalid <a> tags
    return links

def exact_link(link):
    vid_id = link.split("=")
    str_ = "".join(i + "=" for i in vid_id[:2])
    str_new = str_[:-1]
    index = str_new.find("&")
    new_link = "https://www.youtube.com" + str_new[:index]
    return new_link

def download_videos(links):
    # Download each video from the link in the links array
    for link in links:
        yt = pytube.YouTube(link)
        # Download the best quality video
        stream = yt.streams.filter(progressive=True, file_extension="mp4") \
            .order_by("resolution").desc().first()
        try:
            stream.download()
            # Print the downloaded links
            print(f"Downloaded: {link}")
        except Exception as exp:
            print(f"Error downloading {link}: {exp}")

def convert_to_mp3():
    # Convert all downloaded video files to mp3
    for item in glob.glob("*.mp4"):
        video = VideoFileClip(os.path.join(item))
        video.audio.write_audiofile(os.path.join(item[:-4] + ".mp3"))

def cleanup():
    # Remove all downloaded video files
    for item in glob.glob("*.mp4"):
        os.remove(item)

def main():
    url = "https://www.youtube.com/watch?v=GgaYWwuhVGE&list=PLFvYbH6b0oWZ3xrHux5lFnZxL7Mbs-vI5"
    links = extract_links(url)
    download_videos(links)
    convert_to_mp3()
    cleanup()

if __name__ == "__main__":
    main()
